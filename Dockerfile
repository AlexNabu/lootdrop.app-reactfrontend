# base image
FROM node:10.9.0

# install and cache app dependencies
RUN npm install yarn serve -g --silent

# set working directory
RUN mkdir /app
WORKDIR /app

# add `/usr/src/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH
ENV NODE_ENV production

COPY . /app/
RUN yarn

RUN npm run build
RUN npm i http-server -D

# start app
WORKDIR /app
CMD npm run start-sw