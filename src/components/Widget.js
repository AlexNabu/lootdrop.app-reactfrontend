import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Fab from '@material-ui/core/Fab';
import { FilterList as FilterIcon, Search as SearchIcon} from '@material-ui/icons/';

import logo from '../images/logo.png';
import shoe from '../images/shoe.png';
import switch_icon from "../images/icons/switch_icon.png";
import profilePicture from '../images/profile-picture-real.jpg';

class Widget extends Component {

  render() {

    return (
      <div className="container-fluid widgets">
        <div className="row">
          <div className="col-9 pr-0">
            <div className="container-fluid widget" onClick={null}>
              <div className="row">
                <div className="col-9 text" >
                  <span>OFF-WHITE x NIKE…</span><br />

                  <img className="image" src={logo} alt="" /> Solar studios
                  <img className="shoe" src={shoe} width="50px" alt="" />
                </div>

                <div className="col-3">
                  <div className="profile-picture text-center">
                    <img className="profile" src={profilePicture} alt="" onClick={this.openDialog} />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-3 filter">
            <Fab className="filter-button" aria-label="Add">
              <img src={switch_icon} width = "40%" />
            </Fab>
          </div>
        </div>
      </div>
    );
  }
}

export default Widget;
