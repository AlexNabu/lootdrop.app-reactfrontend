import React, { Component } from 'react';

import Button from '@material-ui/core/Button';
import { ArrowLeft as LeftIcon, ArrowRight as RightIcon } from '@material-ui/icons/';
import io from 'socket.io-client';
import SwipeableViews from 'react-swipeable-views';

import Case from './Case.js';
import Loader from './Loader.js';
import MenuTwo from './MenuTwo';


class Cases extends Component {
  state = {
    loading: true,
    categories: '',
    currentCategory: '',
    freeDailyCase: ''
  }

  componentDidMount() {
    this.connectSocket();
  }

  reverseCategory = () => {
    const { currentCategory } = this.state;

    if (currentCategory !== 0) {
      this.setState({
        currentCategory: currentCategory - 1,
      });
    }
  };

  forwardCategory = () => {
    const { currentCategory, categories } = this.state;

    if (currentCategory !== categories.length - 1) {
      this.setState({
        currentCategory: currentCategory + 1,
      });
    }
  };

  handleChangeIndex = (currentCategory) => {
    this.setState({ currentCategory });
  };

  connectSocket() {
    const connection = io.connect('https://lootdrop.app:9090');

    connection.emit('cases');

    connection.on('cases loaded', (data) => {
      const dataJson = JSON.parse(data);
      const { freeDailyCase, cases } = dataJson;
      const categories = [];

      for (let i = 0; i < Object.keys(cases).length; i += 1) {
        const category = cases[Object.keys(cases)[i]];
        category.name = Object.keys(cases)[i];

        categories.push(category);
      }

      this.setState({
        loading: false,
        categories,
        currentCategory: 0,
        freeDailyCase
      });

    });
  }

  render() {
    const { loading, categories, currentCategory } = this.state;

    if (loading) {
      return (
        <Loader />
      );
    }

    return (
      <div className="container-fluid p-0">
        <div className="cases">
          <MenuTwo />
          <div className="categories">
            <Button onClick={this.reverseCategory} disabled={currentCategory === 0 && true}>
              <LeftIcon className={`arrow-icon ${currentCategory === 0 && 'disable-color'}`} />
            </Button>

            <span>{categories[currentCategory].name}</span>

            <Button onClick={this.forwardCategory} disabled={currentCategory === (categories.length - 1) && true}>
              <RightIcon className={`arrow-icon ${currentCategory === (categories.length - 1) && 'disable-color'}`} />
            </Button>
          </div>

          <SwipeableViews index={currentCategory} onChangeIndex={this.handleChangeIndex}>
            {categories.map(category => (
              <div className="row m-1">
                {category.items.map(item => (
                  <Case name={item.name} price={item.price} />
                ))}
              </div>
            ))}
          </SwipeableViews>
        </div>
      </div>
    );
  }
}

export default Cases;
