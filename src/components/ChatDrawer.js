import React, { Component } from 'react';

import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { QuestionAnswer as ChatIcon, Close as CloseIcon, Inbox as InboxIcon} from '@material-ui/icons/';

import Fab from '@material-ui/core/Fab';
import chat_icon from "../images/icons/chat_icon.png";
import drop_icon from "../images/icons/drop_icon.png";

import Chat from './chatdrawer/Chat';
import Drop from './chatdrawer/Drop';

import Loader from './Loader';

class ChatDrawer extends Component {
  state = {
    loading: false,
    chatOpen: false,
    open: 'chat'
  }

  toggleDrawer = open => () => {
    this.setState({
      chatOpen: open
    });
  };

  setChatView = view => () => {
    this.setState({
      open: view
    });
  }

  renderChatMenu() {
    const { open } = this.state;

    return (
      <div className="container-fluid">
        <div className="row chat-menu">
          <div className="col-6">
            <Fab variant="extended" aria-label="Chat" className={open === 'chat' ? 'active' : 'non-active'} onClick={this.setChatView('chat')}>
              <img src={chat_icon} width="18%" className="icon" />
              Chat
            </Fab>
          </div>

          <div className="col-6">
            <Fab variant="extended" aria-label="Drop" className={open === 'drop' ? 'active' : 'non-active'} onClick={this.setChatView('drop')}>
            <img src={drop_icon} width="18%" className="icon" />
              Drop
            </Fab>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { loading, chatOpen, open } = this.state;

    if (loading) {
      return (
        <Loader />
      );
    }

    return [
      <div className="chat-button">
        <Fab color="primary" aria-label="Add" onClick={this.toggleDrawer(!chatOpen)}>
        <img src={chat_icon} width="40%" className="icon" />
        </Fab>
      </div>,

      <div className="chat-drawer">
        {chatOpen && this.renderChatMenu()}

        <SwipeableDrawer
          classes={{ paper: 'container-fluid chat-view'}}
          anchor="bottom"
          open={chatOpen}
          onClose={this.toggleDrawer(false)}
          onOpen={this.toggleDrawer(true)}
          transitionDuration={200}
          disableBackdropTransition
        >
          { open === 'chat' ? <Chat /> : <Drop /> }

          <div className="row footer">
            <div className="col-2 close-button">
              <Fab color="primary" aria-label="Add" onClick={this.toggleDrawer(!chatOpen)}>
                <CloseIcon />
              </Fab>
            </div>

            <div className="col-10 message">
              <input className="message-input" type="text" placeholder="Send message..." />
            </div>
          </div>
        </SwipeableDrawer>
      </div>
    ];
  }
}

export default ChatDrawer;
