import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Loader from './Loader.js';
import levelImage from '../images/level.png';


class MenuNav extends Component {
  state = {
    loading: false
  }

  render() {
    const { loading } = this.state;

    if (loading) {
      return (
        <Loader />
      );
    }

    return (
      <div className="chats">
      <div className="drawer-view">
        <ul>
          <li><a href="">Cases</a></li>
          <li><a href="">Inventory</a></li>
          <li><a href="">Profile</a></li>
          <li><a href="">Balance</a></li>
          <li><a href="">Leaderbord</a></li>
        </ul>

        <div className="points">

          <div className="amount offset-3">
            300 clout points
          </div>
        </div>

        <div className="foot">
          <div className="level text-center">
            <img src={levelImage} className="level" alt="Level" />

            <span>LvL 15</span>
          </div>

          <div className="progress-bar">
            <div className="filled"></div>
          </div>

          <div className="text-center progress-text">
            1500/4000XP
          </div>
        </div>
      </div>

      </div>
    );
  }
}

export default MenuNav;
