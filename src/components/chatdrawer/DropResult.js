import React, { Component } from 'react';
import PropTypes from 'prop-types';

import logo from '../../images/logo.png';
import shoe from '../../images/shoe.png';

class DropResult extends Component {
  static propTypes = {
    message: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired
  }

  render() {
    const { message, user, time } = this.props;

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 ">
            <div className="container-fluid widget" onClick={null}>
              <div className="row">
                <div className="col-9 text">
                  <span>OFF-WHITE x NIKE…</span><br />

                  <img className="image" src={logo} alt="" /> Solar studios
                  <img className="shoe" src={shoe} width="50px" alt="" />
                </div>

                <div className="col-3 amount-p">
                  <div className="amount text-center">
                    12
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DropResult;
