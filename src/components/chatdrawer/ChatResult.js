import React, { Component } from 'react';
import PropTypes from 'prop-types';

import logo from '../../images/logo.png';
import levelImage from '../../images/level.png';
import profilePicture from '../../images/profile-picture-real.jpg';


class ChatResult extends Component {
  static propTypes = {
    message: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired
  }

  render() {
    const { message, user, time } = this.props;

    return (
      <div className="chat-result">
        <div className="row">
          <div className="col-2">
            <img src={profilePicture} alt="logo" />
          </div>

          <div className="col-10 message">
            <span className="username">{user}</span>
            <span className="playerLevel sub10">10</span>
            {/* // <img src={levelImage} className="level" alt="Level" /> */}
            <p>{message}</p>

            <div className="text-right">
              <p>{time}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ChatResult;
