import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Loader from '../Loader.js';
import ChatResult from './ChatResult.js';

class Chat extends Component {
  state = {
    loading: false
  }

  render() {
    const { loading } = this.state;

    if (loading) {
      return (
        <Loader />
      );
    }

    return (
      <div className="chats">
        <ChatResult message="Lorem ipsum dolor sit amet, consectetur adipiscing elit," user="Peter" time="16:06" />
        <ChatResult message="Lount ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud" user="jack" time="16:07" />
        <ChatResult message="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud" user="Peter" time="17:06" />
      </div>
    );
  }
}

export default Chat;
