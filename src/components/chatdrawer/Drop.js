import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Loader from '../Loader';
import DropResult from './DropResult';

class Drop extends Component {
  state = {
    loading: false
  }

  render() {
    const { loading } = this.state;

    if (loading) {
      return (
        <Loader />
      );
    }

    return (
      <div className="drop">
        <DropResult />
        <DropResult />
        <DropResult />
        <DropResult />
        <DropResult />
        <DropResult />
        <DropResult />
        <DropResult />

      </div>
    );
  }
}

export default Drop;
