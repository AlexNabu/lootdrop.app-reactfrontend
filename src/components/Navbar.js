import React, { Component } from 'react';

import { Menu as MenuIcon, Notifications as NotificationsIcon, Add as AddIcon} from '@material-ui/icons/';
import Drawer from '@material-ui/core/Drawer';
import Badge from '@material-ui/core/Badge';
import Button from '@material-ui/core/Button';
import MenuTwo from './MenuTwo';
import Notification from './Notification';

import logo from '../images/logo.png';
import logoBig from '../images/logo-big.png';
import levelImage from '../images/level.png';

class Navbar extends Component {
  state = {
    drawer: false,
    showNotification: true
  }

  toggleDrawer = open => () => {
    this.setState({
      drawer: open
    });
  };

  hideNotification() {
    this.setState({ showNotification: false });
  }

  render() {
    const { drawer, showNotification } = this.state;

    return (
      <div className="sticky-top">
        <nav className="navbar navbar-expand-lg ">
          <div className="row">
            <div className="col-3 pt-2">
              <a className="navbar-brand" href="/">
                <img className="logo" src={logo} alt="" />
              </a>
            </div>

            {!this.props.user.profile_info ?
              <div className="col-7 pt-3 notification-bell">
                <a id="steamLogin" onClick={this.props._loginButtonClicked}>Sign in through STEAM</a>
              </div>

              :
            <>
              <div className="notification-bell col-2 pt-3">
                <Badge badgeContent={99} classes={{ badge: 'badge-color' }}>
                  <NotificationsIcon />
                </Badge>
              </div>

              <div className="col-5 saldo-field">
                <div className="float-left saldo">
                  ${this.props.user.profile_info.credits}
                </div>

                <div className="float-right plus">
                  <AddIcon />
                </div>
              </div>
            </>
          }

            <div className="col-2 pt-2 menu-container">
              <Button className="menu-button" onClick={this.props.toggleMenu(true)}>
                <MenuIcon />
              </Button>
            </div>
            {/* <div className="col-2 pt-2 menu-container">
              <Button className="menu-button" onClick={this.toggleDrawer(!drawer)}>
                <MenuIcon />
              </Button>
            </div> */}
          </div>

          <Drawer
            anchor="bottom"
            style={{ top: '120px !important'}}
            classes={{ paper: `drawer-view ${showNotification ? 'drawer-top-notification' : 'drawer-top'}` }}
            onClose={this.toggleDrawer(false)}
            onOpen={this.toggleDrawer(true)}
            open={drawer}
            transitionDuration={200}
            disableBackdropTransition>

            <div tabIndex={0} role="button">
              <div className="head">
                <img className="logo" src={logoBig} alt="" />
              </div>

              <ul>
                <li><a href="">Cases</a></li>
                <li><a href="">Inventory</a></li>
                <li><a href="">Profile</a></li>
                <li><a href="">Balance</a></li>
                <li><a href="">Leaderbord</a></li>
              </ul>

              <div className="points">

                <div className="amount offset-3">
                  300 clout points
                </div>
              </div>

              <div className="foot">
                <div className="level text-center">
                  <img src={levelImage} className="level" alt="Level" />

                  <span>LvL 15</span>
                </div>

                <div className="progress-bar">
                  <div className="filled"></div>
                </div>

                <div className="text-center progress-text">
                  1500/4000XP
                </div>
              </div>
            </div>
          </Drawer>
        </nav>

        {showNotification && <Notification closeFunction={() => this.hideNotification()} />}
      </div>
    );
  }
}

export default Navbar;
