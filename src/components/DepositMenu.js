import React, { Component } from 'react';

import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { QuestionAnswer as ChatIcon, Close as CloseIcon, Inbox as InboxIcon} from '@material-ui/icons/';

import Fab from '@material-ui/core/Fab';
import chat_icon from "../images/icons/chat_icon.png";
import drop_icon from "../images/icons/drop_icon.png";
// import profile pic dont forget.

import Chat from './chatdrawer/Chat';
import MenuNav from './MenuNav.js';
import Drop from './chatdrawer/Drop';

import Loader from './Loader';

class DepositMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      chatOpen: props.toggleMenu,
      open: 'chat',
    }
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      chatOpen: newProps.toggleMenu
    });
  }


  toggleDrawer = open => () => {
    this.setState({
      chatOpen: open
    });
  };

  setChatView = view => () => {
    this.setState({
      open: view
    });
  }

  renderChatMenu() {
    const { open } = this.state;

    return (
      <div className="container-fluid">
        <div className="row chat-menu">
          {/* <div className="col-6">
            <Fab variant="extended" aria-label="Chat" className={open === 'chat' ? 'active' : 'non-active'} onClick={this.setChatView('chat')}>
              <img src={chat_icon} width="18%" className="icon" />
              Chat
            </Fab>
          </div>

          <div className="col-6">
            <Fab variant="extended" aria-label="Drop" className={open === 'drop' ? 'active' : 'non-active'} onClick={this.setChatView('drop')}>
            <img src={drop_icon} width="18%" className="icon" />
              Drop
            </Fab>
          </div> */}

          <div className="col-12">
            <div class="headAvatar"><img class="logo"
            src={(this.props.user.profile_info) ?
               this.props.user.profile_info.steam_profile_data.avatarfull :
                "/static/media/logo-big.2cb5ca0d.png"} alt=""
            /></div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { loading, chatOpen, open } = this.state;

    if (loading) {
      return (
        <Loader />
      );
    }

    return (
      <div className="chat-drawer">
        {chatOpen && this.renderChatMenu()}

        <SwipeableDrawer
          classes={{ paper: 'chat-view menuLayout DepositMenu'}}
          anchor="bottom"
          open={chatOpen}
          onClose={this.toggleDrawer(false)}
          onOpen={this.toggleDrawer(true)}
          transitionDuration={200}
          disableBackdropTransition
        >
          <div class="crypto-deposit-flexy-divide">
            <div class="top">

              {/* <div class="spinner-head">
                <span>Ready to receive...</span>
              </div> */}

              <div class="bottom">
                <div class="address-info">
                  <div class="address-label">Your BTC deposit address : </div>
                  <div class="address-box"><div class="address">3LWWtngp7G16E1EERf8Nh4dUyfASb8iG33</div></div>
                  <button class="theme-button">copy address</button>
                </div>
              </div>

              <div class="spinner">
                {/* <span class="spinner-label">Ready to receive</span> */}
                <svg class="lds-microsoft" width="100%" height="100%" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><g transform="rotate(0)"><circle cx="69.834" cy="65.219" fill="#456caa" r="3" transform="rotate(102.105 50 50)">
                  <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="2.5s" begin="0s"></animateTransform>
                  </circle><circle cx="65.219" cy="69.834" fill="#88a2ce" r="3" transform="rotate(117.722 50 50)">
                    <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="2.5s" begin="-0.062s"></animateTransform>
                  </circle><circle cx="59.567" cy="73.097" fill="#c2d2ee" r="3" transform="rotate(134.517 50 50)">
                    <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="2.5s" begin="-0.125s"></animateTransform>
                  </circle><circle cx="53.263" cy="74.786" fill="#fefefe" r="3" transform="rotate(151.746 50 50)">
                    <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="2.5s" begin="-0.187s"></animateTransform>
                  </circle><circle cx="46.737" cy="74.786" fill="#456caa" r="3" transform="rotate(169.707 50 50)">
                    <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="2.5s" begin="-0.25s"></animateTransform>
                  </circle><circle cx="40.433" cy="73.097" fill="#88a2ce" r="3" transform="rotate(187.55 50 50)">
                    <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="2.5s" begin="-0.312s"></animateTransform>
                  </circle><circle cx="34.781" cy="69.834" fill="#c2d2ee" r="3" transform="rotate(205.556 50 50)">
                    <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="2.5s" begin="-0.375s"></animateTransform>
                  </circle><circle cx="30.166" cy="65.219" fill="#fefefe" r="3" transform="rotate(222.87 50 50)">
                    <animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;360 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="2.5s" begin="-0.437s"></animateTransform>
                  </circle><animateTransform attributeName="transform" type="rotate" calcMode="spline" values="0 50 50;0 50 50" times="0;1" keySplines="0.5 0 0.5 1" repeatCount="indefinite" dur="2.5s"></animateTransform></g>
                </svg>
              </div>
            </div>

          </div>
        </SwipeableDrawer>
      </div>
    );
  }
}

export default DepositMenu;
