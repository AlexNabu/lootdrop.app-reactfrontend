import React, { Component } from 'react';

import Fab from '@material-ui/core/Fab';
import { FilterList as FilterIcon, Search as SearchIcon} from '@material-ui/icons/';

class Search extends Component {

  render() {

    return (
      <div className="container-fluid search">
        <div className="row">
          <div className="col-9">
            <input type="text" name="search" placeholder="Search case.." />
          </div>

          <div className="col-3 filter">
            <Fab className="filter-button" aria-label="Add">
              <FilterIcon />
            </Fab>
          </div>
        </div>
      </div>
    );
  }
}

export default Search;
