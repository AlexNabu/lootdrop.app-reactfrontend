import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { CardGiftcard as GiftIcon, Clear as ClearIcon } from '@material-ui/icons/';

class Notification extends Component {
  static propTypes = {
    closeFunction: PropTypes.func.isRequired
  }

  render() {

    return (
      <div className="notification">
        <div className="container-fluid">
          <div className="row">
            <div className="col-2">
              <GiftIcon />
            </div>

            <div className="col-8">
              <p className="text-center">You have a <span>free spin</span> available!</p>
            </div>

            <div className="col-2">
              <ClearIcon onClick={() => this.props.closeFunction()} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Notification;
