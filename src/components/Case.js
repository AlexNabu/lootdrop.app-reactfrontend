import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Fab from '@material-ui/core/Fab';

import casePhoto from '../images/case.png';

class Case extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired
  }

  render() {
    const { name, price } = this.props;

    return (
      <div className="col-6 p-2">
        <div className="case p-2">
          <b>{name}</b>

          <img className="logo m-2" src={casePhoto} alt="" />

          <Fab className="price" variant="extended" aria-label="Delete">
            ${price}
          </Fab>
        </div>
      </div>
    );
  }
}

export default Case;
