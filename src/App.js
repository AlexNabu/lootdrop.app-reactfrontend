import React, { Component } from 'react';

import Navbar from './components/Navbar';
import Widget from './components/Widget';
import ChatDrawer from './components/ChatDrawer';
import Cases from './components/Cases';
import MenuTwo from './components/MenuTwo';
import DepositMenu from './components/DepositMenu';

import io from 'socket.io-client';

class App extends Component {

  state = {
    loading: true,
    user : {},
    categories: '',
    currentCategory: '',
    freeDailyCase: ''
  }

  _apiHost = 'https://lootdrop.app:9090';

  toggleDraw = false;

  componentDidMount() {
    this.connectSocket();

    // show cases
    window.socket.emit('cases');
  }

  render() {

    return (
      <div>
        <Navbar
          toggleMenu = {this.toggleMenu}
          _loginButtonClicked = {this._loginButtonClicked.bind(this)}
          user = {this.state.user}
        />

        <Widget />
        {/* <Search /> */}

        <Cases />

        <ChatDrawer />
        <MenuTwo
          toggleMenu = {this.toggleDraw}
          user = {this.state.user}
         />

         <DepositMenu
           toggleMenu = {this.toggleDraw}
           user = {this.state.user}
          />
      </div>
    );
  }

  connectSocket() {
    window.socket = io.connect('https://lootdrop.app:9090');

    window.socket.reconnect = function () {
      console.log('reconnecting to api...');
      this.disconnect();
      this.connect();
    }

    window.socket.on('reconnect', data => window.socket.reconnect());

    window.socket.on('login...', (userInfo) => {
      console.log(userInfo);

      userInfo.profile_info.steam_profile_data = JSON.parse(userInfo.profile_info.steam_profile_data);

      this.setState({user : userInfo});
    })

    window.socket.on('updatedUserSettings', (settings) => {});

    window.socket.on('openCase', (info) => {});

    window.socket.on('auth client', () => window.open(`https://lootdrop.app:9090/auth/steam`, 'lootdrop.app login', 'width=300,height=250'));

    // show cases
    window.socket.emit('cases');

    window.socket.on('cases loaded', (data) => {
      const dataJson = JSON.parse(data);
      const { freeDailyCase, cases } = dataJson;
      const categories = [];

      for (let i = 0; i < Object.keys(cases).length; i += 1) {
        const category = cases[Object.keys(cases)[i]];
        category.name = Object.keys(cases)[i];

        categories.push(category);
      }

      this.setState({
        loading: false,
        categories,
        currentCategory: 0,
        freeDailyCase
      });

    });
  }

  _loginButtonClicked(e) {
    e.preventDefault();
    let login_window = window.open(`${this._apiHost}/auth/steam`, 'lootdrop.app login', 'width=300,height=250');

    // take for granted we would've loaded api site by now and re-connect so we have session cookie avail
    setTimeout( e => window.socket.reconnect() , 3000);

    return false;
  }

  toggleDrawFunc = () => () => {

  }

  toggleMenu = truth => () => {
    console.log(this);
    this.toggleDraw = !this.toggleDraw;
    this.setState({
      activeView : 'nah'
    })
  }
}

export default App;
